package kr.pe.imarch.des;

import java.util.Arrays;

import kr.pe.imarch.des.bit_operation.BitUtil;
import kr.pe.imarch.des.permutebox.ExpansionBox;
import kr.pe.imarch.des.permutebox.InitialPermutationBox;
import kr.pe.imarch.des.permutebox.InverseInitialPermutationBox;
import kr.pe.imarch.des.permutebox.PermutationBox;
import kr.pe.imarch.des.sbox.SBox;
import kr.pe.imarch.des.util.DataTypeConverter;

public class DES {
	private byte[] KEY;
	
	public DES(String string) {
		// TODO Auto-generated constructor stub
		KEY = new byte[8];
		Arrays.fill(KEY, (byte)0x0);
		
		int len = (string.length() > 8 ? 8 : string.length());
		System.arraycopy(string.getBytes(), 0, KEY, 0, len);
	}

	long feistel(long val, long key) {
		long r = ExpansionBox.run(val);
		long value = r ^ key;
		long ret = SBox.run (value);
		
		return BitUtil.truncate(PermutationBox.run (ret), 32);
	}
	
	public long block_forward (long _data, DESKey keys) {
		long data = InitialPermutationBox.run (_data);
		
		long left = BitUtil.truncate(data >> 32, 32),
			 right = BitUtil.truncate(data, 32);
		
		for (int i = 0; i < 16; i++) {
			long key = keys.getKey(i);
			
			long ltmp = right,
				 rtmp = left ^ feistel(right, key);
			
			left = BitUtil.truncate(rtmp, 32);
			right = BitUtil.truncate(ltmp, 32);
		}
		
		data = (left << 32) | right;
		return InverseInitialPermutationBox.run(data);
	}
	
	public long[] array_forward (long[] data, DESKey key) {
		long[] ret = new long[data.length];
		
		
		for (int i = 0; i < data.length; i++) {
			ret[i] = block_forward(data[i], key);
		}
		
		return ret;
	}
	
	public byte[] encrypt(byte[] data) {
		DESKey keys = new DESKey (KEY, new DESLeftShift ());
		
		long[] input = DataTypeConverter.byteToLong(data);
		return DataTypeConverter.longToByte(array_forward(input, keys));
	}
	
	public byte[] decrypt(byte[] data) {
		DESKey keys = new DESKey (KEY, new DESRightShift ());
		
		long[] input = DataTypeConverter.byteToLong (data);
		return DataTypeConverter.longToByte(array_forward(input, keys));
	}
}
