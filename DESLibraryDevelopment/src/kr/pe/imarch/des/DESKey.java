package kr.pe.imarch.des;

import kr.pe.imarch.des.bit_operation.BitUtil;
import kr.pe.imarch.des.bit_operation.ShiftOperator;
import kr.pe.imarch.des.permutebox.KeyCompressionBox;
import kr.pe.imarch.des.permutebox.ParityBitDropBox;

class DESKey {
	long[] keys;
	
	public DESKey (byte[] key, ShiftOperator op) {			
		long keyval = 0;
		for (int i = 0; i < 7; i++) {
			keyval = (keyval << 8) | key[i];
		}
		
		keys = new long[16];
	
		keyval = ParityBitDropBox.run (keyval);
		
		long l = BitUtil.truncate(keyval >> 28, 28),
			 r = BitUtil.truncate(keyval, 28); 
		
		for (int i = 0; i < 16; i++) {
			l = op.shift (l, i, 28);
			r = op.shift (r, i, 28);
			
			keys[i] = KeyCompressionBox.run(l << 28 | r);
		}
	}
	
	public long getKey(int idx) {
		return keys[idx];
	}
}
