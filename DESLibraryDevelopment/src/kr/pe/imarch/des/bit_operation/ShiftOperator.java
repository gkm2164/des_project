package kr.pe.imarch.des.bit_operation;

public interface ShiftOperator {
	public long shift(long number, int current_idx, int bitlength);
}
