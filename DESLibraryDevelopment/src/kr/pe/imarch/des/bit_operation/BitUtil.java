package kr.pe.imarch.des.bit_operation;

public class BitUtil {
	public static long truncate(long num, int cnt) {
		return num & ((1l << cnt) - 1);
	}
	
	public static long getBit (long num, long pos) {
		return (num >> pos) & 0x1;
	}
}
