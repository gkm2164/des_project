package kr.pe.imarch.des;

import kr.pe.imarch.des.bit_operation.BitUtil;
import kr.pe.imarch.des.bit_operation.ShiftOperator;

class DESRightShift implements ShiftOperator {
	byte[] shiftcount = { 0, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1 };

	public long shift(long number, int current_idx, int bitlength) {
		int shiftcnt = shiftcount[current_idx];

		long left = BitUtil.truncate(number >> shiftcnt, bitlength - shiftcnt),
			 right = BitUtil.truncate(number, shiftcnt);

		long ret = (right << (bitlength - shiftcnt)) | left;

		return ret;
	}
}
