package kr.pe.imarch.des.util;

import java.nio.ByteBuffer;
import java.util.Arrays;

public class DataTypeConverter {
	static private int floor(int num, int unit) {
		
		return (num / unit) * unit;
	}
	
	static private int ceil(int num, int unit) {
		
		return floor(num + unit - 1, unit);
	} 
	
	static public long[] byteToLong(byte[] data) {
		
		int size = ceil(data.length, Long.BYTES);
		
		byte[] padding = new byte[size];
		long[] ret = new long[size / Long.BYTES];
		
		Arrays.fill(padding, (byte)0x0);
		
		System.arraycopy(data, 0, padding, 0, data.length);
		
		ByteBuffer.wrap(padding)
				  .asLongBuffer()
				  .get(ret);
		
		return ret;
	}
	
	static public byte[] longToByte(long[] data) {
		int size = data.length * Long.BYTES;
		
		byte[] ret = new byte[size];
		
		ByteBuffer bbuf = ByteBuffer.allocate(size);
		bbuf.asLongBuffer().put(data);
		bbuf.get(ret);
		
		return ret;
	}
}
