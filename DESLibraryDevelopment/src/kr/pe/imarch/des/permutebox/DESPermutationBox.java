package kr.pe.imarch.des.permutebox;

import kr.pe.imarch.des.bit_operation.BitUtil;

class DESPermutationBox {
	/* This class should be inherited in this package and should have permutation box! */
	
	protected static long
	permute(long input, byte[] box,
			int bitlength) {
		long ret = 0;
		for (int i = 0; i < box.length; i++) {
			ret = (ret << 1) | BitUtil.getBit(input, bitlength - box[i]); 
		}
		return ret;
	} 
}
