package kr.pe.imarch.des;

import kr.pe.imarch.des.bit_operation.BitUtil;
import kr.pe.imarch.des.bit_operation.ShiftOperator;

class DESLeftShift implements ShiftOperator {
	byte[] shiftcount = { 1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1 };

	public long shift(long number, int current_idx, int bitlength) {
		// TODO Auto-generated method stub
		int shiftcnt = shiftcount[current_idx];
		long left = BitUtil.truncate(number >> (bitlength - shiftcnt), shiftcnt),
			 right = BitUtil.truncate(number, bitlength - shiftcnt);
		
		return (right << shiftcnt) | left;
	}
}
